//
//  DetailViewModel.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-10.
//

import SwiftUI
enum DetailState {
    case loading
    case failed(Error)
    case loaded(WeatherDataModel)
}

public class DetailViewModel: ObservableObject {
    
    private var requestService = RequestServices()

    @Published private(set) var state = DetailState.loading
    @Published public var dataModel: WeatherDataModel?
    
    public func getDetails(for woeid: Int){
        state = .loading
        let components = URLComponents(string: "\(Const.detailsUrlString)\(woeid)")!
        let url = components.url!

        requestService.getDetailsRequest(url: url, completion: { (result, error) in
            if let result = result {
                for  res in result {
                    if res.key == "consolidated_weather"{
                        if let weatherObjects = res.value as? [[String : Any]]{
                            do {
                                let jsonData = try JSONSerialization.data(withJSONObject:weatherObjects)
                                self.weatherObjects(result:try JSONDecoder().decode([WeatherDataModel].self, from: jsonData))
                            }catch{
                                self.state = .failed(error)
                            }
                        }
                    }
                }
            }else if let error = error {
                self.state = .failed(error)
            }
        })
    }
}


extension DetailViewModel {
    func weatherObjects(result: [WeatherDataModel]) {
        //Get tomorrow object
        for res in result {
            if (res.applicable_date == tomorrowDateAsString()){
                self.dataModel = res
                DispatchQueue.main.async {
                    self.state = .loaded(self.dataModel!)
                }
            }
        }
    }
    
    func tomorrowDateAsString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateTomorrow = dateFormatter.string(from: Date.tomorrow)
        return dateTomorrow
    }
}
