//
//  ContentViewModel.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-06.
//

import SwiftUI

public struct Const {
    public static var arrayOfCities = ["Gothenburg", "Stockholm", "Mountain View", "London", "New York", "Berlin"]
    public static var searchUrlString = "https://www.metaweather.com/api/location/search/"
    public static var detailsUrlString = "https://www.metaweather.com/api/location/"
    public static var weatherImageUrlString =  "https://www.metaweather.com//static/img/weather/png/"
    public static var celDegreeSimbol = "°C"
}
enum ContentState {
    case loading
    case failed(Error)
    case loaded([Int:String])
}
public class ContentViewModel: ObservableObject {
    @Published private(set) var state = ContentState.loading

    var locations: [Int:String] = [:]
    private var requestService = RequestServices()
    private let group = DispatchGroup()
    
    public func getWoeid(for array:[String] = Const.arrayOfCities){
        state = .loading
        for city in array {
            group.enter()
            var components = URLComponents(string: Const.searchUrlString)!
            components.queryItems = [URLQueryItem(name: "query", value: city)]
            let url = components.url!

            requestService.getRequest(url: url, completion: { (result, error) in
                if let result = result {
                    self.appendLocationElement(result: result)
                } else if let error = error {
                    DispatchQueue.main.async {
                        self.state = .failed(error)
                    }
                }
                self.group.leave()
            })
        }
        group.notify(queue: DispatchQueue.main) {
            self.storeLocationsOfIntrest()
        }
    }
}

extension ContentViewModel {
    func appendLocationElement(result: [String: Any]) {
        var woeid: Int?
        var title: String?
        for  res in result {
            if res.key == "woeid"{
                woeid = res.value as? Int
            }
            if res.key == "title" {
                title = res.value as? String
            }
        }
        if let locId = woeid  {
            self.locations[locId] = title
        }
    }
    
    func storeLocationsOfIntrest() {
//        let storedPreferences = StoredPreferences()
//        storedPreferences.storeLocationsOfIntrest(as: self.locations)
        self.state = .loaded(self.locations)
    }
    
    func retreaveLocationsOfIntrest() {
        let storedPreferences = StoredPreferences()
        self.locations = storedPreferences.readLocationsOfInterest()
    }
}
