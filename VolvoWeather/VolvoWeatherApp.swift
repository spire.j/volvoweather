//
//  VolvoWeatherApp.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-06.
//

import SwiftUI

@main
struct VolvoWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ContentViewModel())
        }
    }
}
