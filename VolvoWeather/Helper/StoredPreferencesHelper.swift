//
//  StoredPreferencesHelper.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-06.
//

import Foundation
public var locationsOfInterest: [Int:String] = [:]
struct StoredPreferences {
    let userDefaults = UserDefaults.standard
    public static let LocationOfInterestKey: String = "IntterestLocationKey"

    public func storeLocationsOfIntrest(as element: [Int: String]){
        userDefaults.set(element, forKey: StoredPreferences.LocationOfInterestKey)
    }
    
    public func readLocationsOfInterest() -> [Int:String]{
        if let myKey = userDefaults.object(forKey: StoredPreferences.LocationOfInterestKey) as? [Int:String]{
            return myKey
        }
        return [:]
    }
}
