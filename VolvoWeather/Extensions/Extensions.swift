//
//  Extensions.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-10.
//

import SwiftUI

extension Date {
   static var tomorrow:  Date { return Date().dayAfter }
   static var today: Date {return Date()}
   var dayAfter: Date {
      return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
   }
}

