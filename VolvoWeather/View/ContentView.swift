 //
//  ContentView.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-06.
//

import SwiftUI

struct ContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel: ContentViewModel

    init(viewModel: ContentViewModel){
        self.viewModel = viewModel
        self.viewModel.getWoeid()
    }

    public var body: some View {
        switch viewModel.state {
        case .loading:
            ProgressView()
        case .failed(_):
            //Show error view
            AnyView(EmptyView())
        case .loaded(let locations):
            HStack() {
                NavigationView {
                    content(locations: locations)
                }
            }
        }
    }
    
    private func content(locations: [Int:String]) -> some View {
        VStack() {
            ForEach(locations.sorted(by: >), id: \.key) { key, value in
                self.bodyHelper(element: CityInfoDetails(title: value, woeid: key))
            }
            Spacer()
        }
    }
        
    private func bodyHelper(element: CityInfoDetails) -> AnyView {
        return AnyView(
            NavigationLink(destination:DetailView(viewModel: DetailViewModel(), item: element)){
                HStack(){
                    Text(element.title)
                        .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                        .shadow(radius: 10.0, x: 20, y: 10)
                    Spacer()
                }
            })
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: ContentViewModel())
    }
}
