//
//  DetailView.swift
//  VolvoWeather
//
//  Created by Spire Jankulovski on 2021-10-10.
//
import SwiftUI

struct DetailView: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel: DetailViewModel
    @State private var image: UIImage = UIImage()
    
    let item: CityInfoDetails

    init(viewModel: DetailViewModel, item: CityInfoDetails){
        self.viewModel = viewModel
        self.item = item
    }

    func getDetails() {
        self.viewModel.getDetails(for: self.item.woeid)
    }
    
    func imageFromAbbreviation(abbrv: String) -> UIImage {
        let url = URL(string: Const.weatherImageUrlString+abbrv + ".png")
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url!){ 
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)!
                }
            }
        }

        return self.image
    }
    
    var body: some View {
        switch viewModel.state {
        case .loading:
            ProgressView().onAppear {
                self.getDetails()
            }
        case .failed(_):
            //Show error view
            AnyView(EmptyView())
        case .loaded(let weatherDetails):
            content(weatherDetails: weatherDetails)
        }
    }

    private func content(weatherDetails: WeatherDataModel) -> some View {
        VStack() {
            HStack() {
                Spacer()
                Text(weatherDetails.applicable_date)
                    .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                    .padding([.leading, .trailing, .top, .bottom], 10)
                Spacer()
                Text(weatherDetails.weather_state_name)
                    .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                    .padding([.leading, .trailing, .top, .bottom], 10)
                Spacer()
            }

            HStack() {
                Image(uiImage: imageFromAbbreviation(abbrv: weatherDetails.weather_state_abbr))
                    .resizable()
                    .frame(width: 160, height: 160)
                    .aspectRatio(contentMode: .fit)
            }
            
            HStack(){
                Spacer()
                VStack() {
                    Text("Min")
                        .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                    Text(String(Int(weatherDetails.min_temp.rounded())) + Const.celDegreeSimbol)
                        .font(.body).foregroundColor(Color.blue)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                }
                Spacer()

                VStack() {
                    Text("Daily")
                        .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                    Text(String(Int(weatherDetails.the_temp.rounded())) + Const.celDegreeSimbol)
                        .font(.body).foregroundColor(Color.yellow)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                }
                Spacer()
                
                VStack() {
                    Text("Max")
                        .font(.body).foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                    Text(String(Int(weatherDetails.max_temp.rounded())) + Const.celDegreeSimbol)
                        .font(.body).foregroundColor(Color.red)
                        .padding([.leading, .trailing, .top, .bottom], 10)
                }
                Spacer()
            }
            Spacer()
        }.navigationTitle(item.title)
        .navigationBarTitleDisplayMode(.inline)
    }
}
