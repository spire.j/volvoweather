//
//  VolvoWeatherTests.swift
//  VolvoWeatherTests
//
//  Created by Spire Jankulovski on 2021-10-06.
//

import XCTest
@testable import VolvoWeather

class VolvoWeatherTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testCodableWeatherDataModel() throws {
        // Creating an instance of the Codable type that
        // we want to test:
        let weatherDataModel = WeatherDataModel(
            id: 1,
            weather_state_name: "Clear",
            weather_state_abbr: "c",
            wind_direction_compass: "some",
            created: "2021-10-10",
            applicable_date: "2021-10-10",
            min_temp: 12.123,
            max_temp: 22.123,
            the_temp: 20.123,
            wind_speed: 23.0,
            wind_direction: 56.0,
            air_pressure: 33.12,
            humidity: 76,
            visibility: 6.5,
            predictability: 35
        )

        // Encoding the instance into Data, then immediately
        // decoding that data back into a new instance:
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(weatherDataModel) {
        let decoded = try JSONDecoder().decode(WeatherDataModel.self, from: encoded)
            // Asserting that the two instances are equal:
            let isEqual = weatherDataModel == decoded
            XCTAssert(isEqual) // passes
            XCTAssertEqual(decoded.predictability, 35)
            XCTAssertEqual(decoded.weather_state_abbr, "c")
            XCTAssertEqual(decoded.id, 1)
        }
    }

    func testCodableCityInfoDetails() throws {
        // Creating an instance of the Codable type that
        // we want to test:
        let cityInfoDetails = CityInfoDetails(
            title: "Lund",
            woeid: 223654,
            location_type: "City",
            latt_long: "some"
        )

        // Encoding the instance into Data, then immediately
        // decoding that data back into a new instance:
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(cityInfoDetails) {
        let decoded = try JSONDecoder().decode(CityInfoDetails.self, from: encoded)
            // Asserting that the two instances are equal:
            let isEqual = cityInfoDetails == decoded
            XCTAssert(isEqual) // passes
            XCTAssertEqual(decoded.title, "Lund")
            XCTAssertEqual(decoded.woeid, 223654)
            XCTAssertEqual(decoded.location_type, "City")
        }
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
