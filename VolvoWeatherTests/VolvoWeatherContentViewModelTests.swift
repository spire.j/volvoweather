//
//  VolvoWeatherContentViewModelTests.swift
//  VolvoWeatherTests
//
//  Created by Spire Jankulovski on 2021-10-10.
//

import XCTest
@testable import VolvoWeather

final class VolvoWeatherContentViewModelTests: XCTestCase {
    var cvm: ContentViewModel!
    var dvm: DetailViewModel!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        cvm = ContentViewModel()
        dvm = DetailViewModel()
    }
    func testAppendLocationElement() {
      // given
        cvm.appendLocationElement(result: [ "title" : "Lund", "woeid" : 223654, "location_type" : "City", "latt_long" : "some"])
        // then
        XCTAssertEqual(cvm.locations, [223654:"Lund"])
    }

    func testWeatherObjectsCreation() {
      // given
        cvm.appendLocationElement(result: [ "title" : "Lund", "woeid" : 223654, "location_type" : "City", "latt_long" : "some"])
        // then
        XCTAssertEqual(cvm.locations, [223654:"Lund"])
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        cvm = nil
        try super.tearDownWithError()
    }

}
