//
//  VolvoWeatherDetailedViewModelTests.swift
//  VolvoWeatherTests
//
//  Created by Spire Jankulovski on 2021-10-10.
//

import XCTest
@testable import VolvoWeather

final class VolvoWeatherDetailedViewModelTests: XCTestCase {
    var dvm: DetailViewModel!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        dvm = DetailViewModel()
    }

    func testWeatherObjectsCreation() {
        let weatherDataModel = WeatherDataModel(
            id: 1,
            weather_state_name: "Clear",
            weather_state_abbr: "c",
            wind_direction_compass: "some",
            created: "2021-10-11",
            applicable_date: dvm.tomorrowDateAsString(),
            min_temp: 12.123,
            max_temp: 22.123,
            the_temp: 20.123,
            wind_speed: 23.0,
            wind_direction: 56.0,
            air_pressure: 33.12,
            humidity: 76,
            visibility: 6.5,
            predictability: 35
        )

        let weatherDataModel1 = WeatherDataModel(
            id: 1,
            weather_state_name: "Show",
            weather_state_abbr: "c",
            wind_direction_compass: "some",
            created: "2021-10-12",
            applicable_date: "2021-10-12",
            min_temp: 22.123,
            max_temp: 32.123,
            the_temp: 30.123,
            wind_speed: 3.0,
            wind_direction: 16.0,
            air_pressure: 30.12,
            humidity: 69,
            visibility: 6.5,
            predictability: 55
        )

        let weatherDataModel2 = WeatherDataModel(
            id: 1,
            weather_state_name: "Sun",
            weather_state_abbr: "c",
            wind_direction_compass: "some",
            created: "2021-10-13",
            applicable_date: "2021-10-13",
            min_temp: 2.123,
            max_temp: 12.123,
            the_temp: 10.123,
            wind_speed: 50.0,
            wind_direction: 116.0,
            air_pressure: 323.12,
            humidity: 46,
            visibility: 6.5,
            predictability: 39
        )

        dvm.weatherObjects(result: [weatherDataModel,  weatherDataModel1, weatherDataModel2])
        XCTAssertEqual(dvm.dataModel?.air_pressure, 33.12)
        XCTAssertEqual(dvm.dataModel?.weather_state_name, "Clear")
        XCTAssertEqual(dvm.dataModel?.created, "2021-10-11")
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        dvm = nil
        try super.tearDownWithError()
    }

}
